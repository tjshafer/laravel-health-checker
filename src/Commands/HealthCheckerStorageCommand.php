<?php

namespace Myopensoft\HealthChecker\Commands;

use Illuminate\Console\Command;
use Myopensoft\HealthChecker\HealthChecker;

class HealthCheckerStorageCommand extends Command
{
    public $signature = 'health:storage';

    public $description = 'Check storage usage.';

    public function handle()
    {
        $data = [
            'status' => 'ok',
            'access_token' => config('health-checker.access_token'),
            'server_token' => config('health-checker.server_token'),
            'type' => '4',
            'data' => HealthChecker::getFreeDiskPercentage(),
        ];
    }
}
