<?php

namespace Myopensoft\HealthChecker;

class HealthChecker
{
    public static function getLoad($interval = 0): float
    {
        $loads = sys_getloadavg();

        switch (PHP_OS){
            case 'Darwin':
                $core_nums = intval(trim(shell_exec("sysctl -n machdep.cpu.core_count"))) + 1;
                break;
            case 'Linux':
                $core_nums = trim(shell_exec("grep -P '^processor' /proc/cpuinfo|wc -l"));
                break;
            case 'WINNT':
                $core_nums = explode("\n", shell_exec('WMIC CPU Get NumberOfCores'))[1];
                break;
            default:
                $core_nums = 1;
                break;
        }

        return round(($loads[$interval] / $core_nums) * 100, 2);
    }

    public static function getFreeDiskPercentage($dir = '/'): float
    {
        return number_format(disk_free_space($dir) / disk_total_space($dir) * 100, 2);
    }
}
