# Laravel Health Checker

[![Latest Version on Packagist](https://img.shields.io/packagist/v/myopensoft/laravel-health-checker.svg?style=flat-square)](https://packagist.org/packages/myopensoft/laravel-health-checker)
[![Total Downloads](https://img.shields.io/packagist/dt/myopensoft/laravel-health-checker.svg?style=flat-square)](https://packagist.org/packages/myopensoft/laravel-health-checker)

Laravel Health Checker library is for server to send server health info to receiver server and be processed.

## Installation

You can install the package via composer:

```bash
composer require myopensoft/laravel-health-checker
```

You can publish and run the migrations with:

You can publish the config file with:
```bash
php artisan vendor:publish --provider="Myopensoft\HealthChecker\HealthCheckerServiceProvider" --tag="health-checker-config"
```

This is the contents of the published config file:

```php
return [
    'receiver_url' => env('HEALTH_RECEIVER_URL', '127.0.0.1'),
    'verify_ssl' => env('HEALTH_VERIFY_SSL', 'true'),
    'access_token' => env('HEALTH_ACCESS_TOKEN', ''),
    'server_token' => env('HEALTH_SERVER_TOKEN', '')
];
```

## Usage

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](.github/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Muhammad Syafiq Bin Zainuddin](https://github.com/lomotech)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
