<?php

namespace Myopensoft\HealthChecker\Commands;

use Illuminate\Console\Command;

class HealthCheckerWebserviceCommand extends Command
{
    public $signature = 'health:webservice';

    public $description = 'Check webservice availability';

    public function handle()
    {
        $this->comment('All done');
    }
}
