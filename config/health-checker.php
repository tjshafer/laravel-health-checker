<?php

return [
    'receiver_url' => env('HEALTH_RECEIVER_URL', '127.0.0.1'),
    'verify_ssl' => env('HEALTH_VERIFY_SSL', 'true'),
    'access_token' => env('HEALTH_ACCESS_TOKEN', ''),
    'server_token' => env('HEALTH_SERVER_TOKEN', '')
];
